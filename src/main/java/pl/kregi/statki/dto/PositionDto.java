package pl.kregi.statki.dto;

import lombok.Getter;

@Getter
public class PositionDto {
    private String position;
}
