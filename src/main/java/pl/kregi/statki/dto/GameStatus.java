package pl.kregi.statki.dto;

public enum GameStatus {

    YOUR_TURN,
    AWAITING_PLAYERS,
    WAITING_FOR_OPPONENT_MOVE,
    YOU_WON,
    YOU_LOST
}
